#!/bin/sh

. package/info

usage () {
cat <<EOF
Usage: $0 [OPTION]... [TARGET]

Defaults for the options are specified in brackets.

System types:
  --target=TARGET                 configure to run on target TARGET [detected]
  --host=TARGET                   same as --target

Installation directories:
  --prefix=PREFIX                 main installation prefix [/]
  --exec-prefix=EPREFIX           installation prefix for executable files [PREFIX]

Fine tuning of the installation directories:
  --dynlibdir=DIR                 shared library files [PREFIX/lib]
  --bindir=BINDIR                 user executables [EPREFIX/bin]
  --libexecdir=DIR                package-scoped executables [EPREFIX/libexec]
  --libdir=DIR                    static library files [PREFIX/lib/$package]
  --includedir=DIR                C header files [PREFIX/include]
  
  --shebangdir=DIR                absolute path for #! invocations [BINDIR]
  --livedir=DIR                   default live directory [/run/66]
  --with-system-dir=DIR           66 tools root working directory[PREFIX/lib/66]
  --with-user-dir=DIR             66 tools user working directory[.66]
  --with-system-log=DIR           root service log directory[PREFIX/log/66]
  --with-user-log=DIR             user service log directory[.66/log]
  --with-packager-service=DIR     packager service installation directory[/etc/66/service]
  --with-sys-service=DIR          sysadmin service directory[/etc/66/sysadmin/service]
  --with-user-service=DIR         user service directory[.66/service]
  --with-sys-service-conf=DIR     sysadmin service configuration directory[/etc/66/conf]
  --with-user-service-conf=DIR    user service configuration directory[.66/conf]
 
 Do not set an absolute path but a \$HOME relative path for --with-user-dir,  
 --with-user-log, --with-user-service, --with-user-service-conf. The \$HOME prefix 
 will be appened at the pathname automatically in function of the user.
 For example , by default the final path for --with-user-dir will be \$HOME/.66.
 
 --with-packager-service and --with-sys-service directory must be two differents path.
 For example do not set --with-sys-service=/etc/66/service/sysadmin with packager service   
 --with-sys-service=/etc/66/service.
 
 If no --prefix option is given, by default --with-system-dir will be /var/lib/66.

Dependencies:
  --with-sysdeps=DIR            use sysdeps in DIR [PREFIX/lib/skalibs/sysdeps]
  --with-include=DIR             add DIR to the list of searched directories for headers
  --with-lib=DIR                 add DIR to the list of searched directories for static libraries
  --with-dynlib=DIR              add DIR to the list of searched directories for shared libraries
 
Optional features:
  --enable-shared                build shared libraries [disabled]
  --disable-static               do not build static libraries [enabled]
  --disable-allstatic            do not prefer linking against static libraries [enabled]
  --enable-static-libc           make entirely static binaries [disabled]
  --disable-all-pic              do not build static libraries as PIC [enabled]
  --enable-slashpackage[=ROOT]   assume /package installation at ROOT [disabled]
  --enable-absolute-paths        do not rely on PATH to access this package's binaries,
                                 hardcode absolute BINDIR/foobar paths instead [disabled]
EOF
exit 0
}

# Helper functions

# If your system does not have printf, you can comment this, but it is
# generally not a good idea to use echo.
# See http://www.etalabs.net/sh_tricks.html
echo () {
  IFS=" "
  printf %s\\n "$*"
}

quote () {
  tr '\n' ' ' <<EOF | grep '^[-[:alnum:]_=,./:]* $' >/dev/null 2>&1 && { echo "$1" ; return 0 ; }
$1
EOF
  echo "$1" | sed -e "s/'/'\\\\''/g" -e "1s/^/'/" -e "\$s/\$/'/" -e "s#^'\([-[:alnum:]_,./:]*\)=\(.*\)\$#\1='\2#" -e "s|\*/|* /|g"
}

fail () {
  echo "$*"
  exit 1
}

fnmatch () {
  eval "case \"\$2\" in $1) return 0 ;; *) return 1 ;; esac"
}

cmdexists () {
  type "$1" >/dev/null 2>&1
}

trycc () {
  test -z "$CC_AUTO" && cmdexists "$1" && CC_AUTO="$*"
}

stripdir () {
  while eval "fnmatch '*/' \"\${$1}\"" ; do
    eval "$1=\${$1%/}"
  done
}

tryflag () {
  echo "checking whether compiler accepts $2 ..."
  echo "typedef int x;" > "$tmpc"
  if $CC_AUTO $CPPFLAGS_AUTO $CPPFLAGS $CPPFLAGS_POST $CFLAGS_AUTO $CFLAGS $CFLAGS_POST "$2" -c -o /dev/null "$tmpc" >/dev/null 2>&1 ; then
    echo "  ... yes"
    eval "$1=\"\${$1} \$2\""
    eval "$1=\${$1# }"
    return 0
  else
    echo "  ... no"
    return 1
  fi
}

tryldflag () {
  echo "checking whether linker accepts $2 ..."
  echo "typedef int x;" > "$tmpc"
  if $CC_AUTO $CFLAGS_AUTO $CFLAGS $CFLAGS_POST $LDFLAGS_AUTO $LDFLAGS $LDFLAGS_POST -nostdlib "$2" -o /dev/null "$tmpc" >/dev/null 2>&1 ; then
    echo "  ... yes"
    eval "$1=\"\${$1} \$2\""
    eval "$1=\${$1# }"
    return 0
  else
    echo "  ... no"
    return 1
  fi
}


# Actual script

CC_AUTO=
CPPFLAGS_AUTO="-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700 -iquote src/include-local -Isrc/include"
CPPFLAGS_POST="$CPPFLAGS"
CPPFLAGS=
CFLAGS_AUTO="-pipe -Wall"
CFLAGS_POST="$CFLAGS"
CFLAGS=-O2
LDFLAGS_AUTO=
LDFLAGS_POST="$LDFLAGS"
LDFLAGS=
LDFLAGS_NOSHARED=
LDFLAGS_SHARED=-shared
prefix=
exec_prefix='$prefix'
dynlibdir='$prefix/lib'
libexecdir='$exec_prefix/libexec'
bindir='$exec_prefix/bin'
libdir='$prefix/lib/$package'
includedir='$prefix/include'
shebangdir='$bindir'
livedir='/run/66'
system_dir='$prefix/lib/66'
user_dir='.66'
system_log='/var/log/66'
user_log='.66/log'
service_packager='/etc/66/service'
service_sys='/etc/66/sysadmin/service'
service_user='.66/service'
service_sysconf='/etc/66/conf'
service_userconf='.66/conf'
sysdeps='$prefix/lib/skalibs/sysdeps'
manualsysdeps=false
shared=false
static=true
allpic=true
slashpackage=false
abspath=false
sproot=
home=
exthome=
allstatic=true
evenmorestatic=false
addincpath=''
addlibspath=''
addlibdpath=''
vpaths=''
vpathd=''
build=


for arg ; do
  case "$arg" in
    --help) usage ;;
    --prefix=*) prefix=${arg#*=} ;;
    --exec-prefix=*) exec_prefix=${arg#*=} ;;
    --dynlibdir=*) dynlibdir=${arg#*=} ;;
    --libexecdir=*) libexecdir=${arg#*=} ;;
    --bindir=*) bindir=${arg#*=} ;;
    --libdir=*) libdir=${arg#*=} ;;
    --includedir=*) includedir=${arg#*=} ;;
    --shebangdir=*) shebangisdefault=false ; shebangdir=${arg#*=} ;;
    --livedir=*) livedir=${arg#*=} ;;
    --with-system-dir=*) system_dir=${arg#*=} ;;
    --with-user-dir=*) user_dir=${arg#*=} ;;
    --with-system-log=*) system_log=${arg#*=} ;;
    --with-user-log=*) user_log=${arg#*=} ;;
    --with-packager-service=*) service_packager=${arg#*=} ;;
    --with-sys-service=*) service_sys=${arg#*=} ;;
    --with-user-service=*) service_user=${arg#*=} ;;
    --with-sys-service-conf=*) service_sysconf=${arg#*=} ;;
    --with-user-service-conf=*) service_userconf=${arg#*=} ;;
    --with-sysdeps=*) sysdeps=${arg#*=} manualsysdeps=true ;;
    --with-include=*) var=${arg#*=} ; stripdir var ; addincpath="$addincpath -I$var" ;;
    --with-lib=*) var=${arg#*=} ; stripdir var ; addlibspath="$addlibspath -L$var" ; vpaths="$vpaths $var" ;;
    --with-dynlib=*) var=${arg#*=} ; stripdir var ; addlibdpath="$addlibdpath -L$var" ; vpathd="$vpathd $var" ;;
    --enable-shared|--enable-shared=yes) shared=true ;;
    --disable-shared|--enable-shared=no) shared=false ;;
    --enable-static|--enable-static=yes) static=true ;;
    --disable-static|--enable-static=no) static=false ;;
    --enable-allstatic|--enable-allstatic=yes) allstatic=true ;;
    --disable-allstatic|--enable-allstatic=no) allstatic=false ; evenmorestatic=false ;;
    --enable-static-libc|--enable-static-libc=yes) allstatic=true ; evenmorestatic=true ;;
    --disable-static-libc|--enable-static-libc=no) evenmorestatic=false ;;
    --enable-all-pic|--enable-all-pic=yes) allpic=true ;;
    --disable-all-pic|--enable-all-pic=no) allpic=false ;;
    --enable-slashpackage=*) sproot=${arg#*=} ; slashpackage=true ; ;;
    --enable-slashpackage) sproot= ; slashpackage=true ;;
    --disable-slashpackage) sproot= ; slashpackage=false ;;
    --enable-absolute-paths|--enable-absolute-paths=yes) abspath=true ;;
    --disable-absolute-paths|--enable-absolute-paths=no) abspath=false ;;
    --enable-*|--disable-*|--with-*|--without-*|--*dir=*) ;;
    --host=*|--target=*) target=${arg#*=} ;;
    --build=*) build=${arg#*=} ;;
    -* ) echo "$0: unknown option $arg" ;;
    *=*) ;;
    *) target=$arg ;;
  esac
done

# Add /usr in the default default case
if test -z "$prefix" ; then
  if test "$libdir" = '$prefix/lib/$package' ; then
    libdir=/usr/lib/$package
  fi
  if test "$includedir" = '$prefix/include' ; then
    includedir=/usr/include
  fi
  if test "$system_dir" = '$prefix/lib/66' ; then
	system_dir=/var/lib/66
  fi
  if test "$sysdeps" = '$prefix/lib/skalibs/sysdeps' ; then
    sysdeps=/usr/lib/skalibs/sysdeps
  fi
fi

# Expand installation directories
stripdir prefix
for i in exec_prefix dynlibdir libexecdir bindir libdir includedir shebangdir sproot sysdeps system_dir; do
  eval tmp=\${$i}
  eval $i=$tmp
  stripdir $i
done

# Get usable temp filenames
i=0
set -C
while : ; do
  i=$(($i+1))
  tmpc="./tmp-configure-$$-$PPID-$i.c"
  tmpe="./tmp-configure-$$-$PPID-$i.tmp"
  2>|/dev/null > "$tmpc" && break
  2>|/dev/null > "$tmpe" && break
  test "$i" -gt 50 && fail "$0: cannot create temporary files"
done
set +C
trap 'rm -f "$tmpc" "$tmpe"' EXIT ABRT INT QUIT TERM HUP

# Set slashpackage values
if $slashpackage ; then
  home=${sproot}/package/${category}/${package}-${version}
  exthome=${sproot}/package/${category}/${package}
  if $manualsysdeps ; then
    :
  else
    sysdeps=${sproot}/package/prog/skalibs/sysdeps
  fi
  extbinprefix=${exthome}/command
  dynlibdir=${home}/library.so
  bindir=${home}/command
  libdir=${home}/library
  libexecdir=$bindir
  includedir=${home}/include
  if $shebangisdefault ; then
    shebangdir=${extbinprefix}
  fi
  while read dep condvar ; do
    if test -n "$condvar" ; then
      eval "cond=$condvar"
    else
      cond=true
    fi
    if $cond ; then
      addincpath="$addincpath -I${sproot}${dep}/include"
      vpaths="$vpaths ${sproot}${dep}/library"
      addlibspath="$addlibspath -L${sproot}${dep}/library"
      vpathd="$vpathd ${sproot}${dep}/library.so"
      addlibdpath="$addlibdpath -L${sproot}${dep}/library.so"
    fi
  done < package/deps-build
fi

# Find a C compiler to use
if test -n "$target" && test x${build} != x${target} ; then
  cross=${target}-
else
  cross=
fi
echo "checking for C compiler..."
trycc ${cross}${CC}
trycc ${cross}gcc
trycc ${cross}clang
trycc ${cross}cc
test -n "$CC_AUTO" || { echo "$0: cannot find a C compiler" ; exit 1 ; }
echo "  ... $CC_AUTO"
echo "checking whether C compiler works... "
echo "typedef int x;" > "$tmpc"
if $CC_AUTO $CPPFLAGS_AUTO $CPPFLAGS $CPPFLAGS_POST $CFLAGS_AUTO $CFLAGS $CFLAGS_POST -c -o /dev/null "$tmpc" 2>"$tmpe" ; then
  echo "  ... yes"
else
  echo "  ... no. Compiler output follows:"
  cat < "$tmpe"
  exit 1
fi

echo "checking target system type..."
if test -z "$target" ; then
  if test -n "$build" ; then
    target=$build ;
  else
    target=$($CC_AUTO -dumpmachine 2>/dev/null) || target=unknown
  fi
fi
echo "  ... $target"
if test ! -d $sysdeps || test ! -f $sysdeps/target ; then
  echo "$0: error: $sysdeps is not a valid sysdeps directory"
  exit 1
fi
if [ "x$target" != "x$(cat $sysdeps/target)" ] ; then
  echo "$0: error: target $target does not match the contents of $sysdeps/target"
  exit 1
fi

if ! test -z "$PKG_CONFIG" && command -v $PKG_CONFIG > /dev/null ; then
    pkg_config=$PKG_CONFIG
else
    pkg_config=${cross}pkg-config
fi

spawn_lib=$(cat $sysdeps/spawn.lib)
socket_lib=$(cat $sysdeps/socket.lib)
sysclock_lib=$(cat $sysdeps/sysclock.lib)
tainnow_lib=$(cat $sysdeps/tainnow.lib)
timer_lib=$(cat $sysdeps/timer.lib)
util_lib=$(cat $sysdeps/util.lib)
procps_lib=$($pkg_config --libs libprocps)

if $allpic ; then
  tryflag CFLAGS_AUTO -fPIC
fi



tryflag CFLAGS_AUTO -std=c99
tryflag CFLAGS -fomit-frame-pointer
tryflag CFLAGS_AUTO -fno-exceptions
tryflag CFLAGS_AUTO -fno-unwind-tables
tryflag CFLAGS_AUTO -fno-asynchronous-unwind-tables
tryflag CFLAGS_AUTO -Wa,--noexecstack
tryflag CFLAGS -fno-stack-protector
tryflag CPPFLAGS_AUTO -Werror=implicit-function-declaration
tryflag CPPFLAGS_AUTO -Werror=implicit-int
tryflag CPPFLAGS_AUTO -Werror=pointer-sign
tryflag CPPFLAGS_AUTO -Werror=pointer-arith
tryflag CPPFLAGS_AUTO -Wno-unused-value
tryflag CPPFLAGS_AUTO -Wno-parentheses
tryflag CFLAGS_AUTO -ffunction-sections
tryflag CFLAGS_AUTO -fdata-sections

tryldflag LDFLAGS_AUTO -Wl,--sort-section=alignment
tryldflag LDFLAGS_AUTO -Wl,--sort-common

CPPFLAGS_AUTO="${CPPFLAGS_AUTO}${addincpath}"

if $evenmorestatic ; then
  LDFLAGS_NOSHARED=-static
fi

if $shared ; then
  tryldflag LDFLAGS -Wl,--hash-style=both
fi

LDFLAGS_SHARED="${LDFLAGS_SHARED}${addlibdpath}"

if $allstatic ; then
  LDFLAGS_NOSHARED="${LDFLAGS_NOSHARED}${addlibspath}"
  tryldflag LDFLAGS_NOSHARED -Wl,--gc-sections
else
  LDFLAGS_NOSHARED="${LDFLAGS_NOSHARED}${addlibdpath}"
fi

if test -z "$vpaths" ; then
  while read dep ; do
    base=$(basename $dep) ;
    vpaths="$vpaths /usr/lib/$base"
    addlibspath="$addlibspath -L/usr/lib/$base"
  done < package/deps-build  
fi

echo "creating config.mak..."
cmdline=$(quote "$0")
for i ; do cmdline="$cmdline $(quote "$i")" ; done
exec 3>&1 1>config.mak
cat << EOF
# This file was generated by:
# $cmdline
# Any changes made here will be lost if configure is re-run.
target := $target
package := $package
prefix := $prefix
exec_prefix := $exec_prefix
dynlibdir := $dynlibdir
libexecdir := $libexecdir
bindir := $bindir
libdir := $libdir
includedir := $includedir
shebangdir := $bindir
livedir := $livedir
system_dir := $system_dir
user_dir := $user_dir
system_log := $system_log
user_log := $user_log
service_packager := $service_packager
service_sys := $service_sys
service_user := $service_user
service_sysconf := $service_sysconf
service_userconf := $service_userconf
sysdeps := $sysdeps
slashpackage := $slashpackage
sproot := $sproot
version := $version
home := $home
exthome := $exthome
PROCPS_LIB := ${procps_lib}
SPAWN_LIB := ${spawn_lib}
SOCKET_LIB := ${socket_lib}
SYSCLOCK_LIB := ${sysclock_lib}
TAINNOW_LIB := ${tainnow_lib}
TIMER_LIB := ${timer_lib}
UTIL_LIB := ${util_lib}

CC := ${CC_AUTO##${cross}}
CPPFLAGS_AUTO := $CPPFLAGS_AUTO
CPPFLAGS := $CPPFLAGS $CPPFLAGS_POST
CFLAGS_AUTO := $CFLAGS_AUTO
CFLAGS := $CFLAGS $CFLAGS_POST
LDFLAGS_AUTO := $LDFLAGS_AUTO
LDFLAGS := $LDFLAGS $LDFLAGS_POST
LDFLAGS_SHARED := $LDFLAGS_SHARED
LDFLAGS_NOSHARED := $LDFLAGS_NOSHARED
CROSS_COMPILE := ${cross}

vpath lib%.a$vpaths
vpath lib%.so$vpathd
EOF
if $allstatic ; then
  echo ".LIBPATTERNS := lib%.a"
  echo "DO_ALLSTATIC := 1"
else
  echo ".LIBPATTERNS := lib%.so"
fi
if $static ; then
  echo "DO_STATIC := 1"
else
  echo "DO_STATIC :="
fi
if $shared ; then
  echo "DO_SHARED := 1"
else
  echo "DO_SHARED :="
fi
if $allpic ; then
  echo "STATIC_LIBS_ARE_PIC := 1"
else
  echo "STATIC_LIBS_ARE_PIC :="
fi

exec 1>&3 3>&-
echo "  ... done."

echo "creating src/include/${package}/config.h..."
mkdir -p -m 0755 src/include/${package}
exec 3>&1 1> src/include/${package}/config.h
cat <<EOF
/* Copyright (c) 2018 Eric Vidal <eric@obarun.org>
All rights reserved.*/

/* ISC license. */

/* Generated by: $cmdline */

#ifndef ${package_macro_name}_CONFIG_H
#define ${package_macro_name}_CONFIG_H

#define ${package_macro_name}_VERSION "$version"
#define ${package_macro_name}_LIVE "$livedir/"
#define ${package_macro_name}_SYSTEM_DIR "$system_dir/"
#define ${package_macro_name}_LOGGER_SYSDIR "$system_log/"
#define ${package_macro_name}_SERVICE_PACKDIR "$service_packager/"
#define ${package_macro_name}_SERVICE_SYSDIR "$service_sys/"
#define ${package_macro_name}_SERVICE_SYSCONFDIR "$service_sysconf/"

/** Do not use absolute path but a \$HOME relative path
 * The /home/name_of_user prefix will be automatically added to the pathname */
#define ${package_macro_name}_USER_DIR "$user_dir/"
#define ${package_macro_name}_LOGGER_USERDIR "$user_log/"
#define ${package_macro_name}_SERVICE_USERDIR "$service_user/"
#define ${package_macro_name}_SERVICE_USERCONFDIR "$service_userconf/"

EOF
if $slashpackage ; then
  echo "#define ${package_macro_name}_BINPREFIX \"$bindir/\""
  echo "#define ${package_macro_name}_EXTBINPREFIX \"$extbinprefix/\""
  echo "#define ${package_macro_name}_EXTLIBEXECPREFIX \"$extbinprefix/\""
elif $abspath ; then
  echo "#define ${package_macro_name}_BINPREFIX \"$bindir/\""
  echo "#define ${package_macro_name}_EXTBINPREFIX \"$bindir/\""
  echo "#define ${package_macro_name}_EXTLIBEXECPREFIX \"$libexecdir/\""
else
  echo "#define ${package_macro_name}_BINPREFIX \"\""
  echo "#define ${package_macro_name}_EXTBINPREFIX \"\""
  echo "#define ${package_macro_name}_EXTLIBEXECPREFIX \"$libexecdir/\""
fi
echo "#define ${package_macro_name}_LIBEXECPREFIX \"$libexecdir/\""
echo "#define ${package_macro_name}_EXECLINE_SHEBANGPREFIX \"$shebangdir/\""
echo
echo "#endif"
exec 1>&3 3>&-
echo "  ... done."
